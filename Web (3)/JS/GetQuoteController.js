﻿



function getQuote() {


    showLoadingModal();




    var user = JSON.parse(localStorage.getItem("user"));
    var userSignup = JSON.parse(localStorage.getItem("userSignup"));
    var vehicle = "";
    var quoteData = "";
    var cellno = user.cellNumber;

    if (localStorage.getItem("vehicle") == null || localStorage.getItem("vehicle") == "" || localStorage.getItem("vehicle") == "undefined" ) {
        quoteData = {

            "ID_number": user.identityNumber1,

            "FirstName": user.forename1,

            "LastName": user.surname,

            "Initials": userSignup.Initials,

            "CellNumber": "+27" + user.cellNumber.substring(user.cellNumber.length - 9, user.cellNumber.length),

            "CreditCheckDate": "15-Jun-2021",

            "CreditScore": user.creditScore,

            "DateOfBirth": user.dateOfBirth.substring(0, 10),

            "Email": "email@email.com",

            "Gender": user.gender,

            "HomePhone": "+27" + user.cellNumber.substring(user.cellNumber.length - 9, user.cellNumber.length),

            "JudgementAmount": user.judgementsAmount,

            "LapseScore": user.lapseScore,

            "MaritalStatus": "1",//user.maritalStatus.code,

            "NoOfClaims": 0,  // user entry

            "NoOfJudgements": user.noOfJudgements,

            "PrevInsuranceYears": 14, // user entry

            "RetiredYN": "N", // user entry

            "Title": "004",

            "WorkPhone": "+27" + user.cellNumber.substring(user.cellNumber.length - 9, user.cellNumber.length),

            "address": {

                "AreaCode": user.addressList[0].province.code,

                "CountryCode": "ZA",

                "Line1": user.addressList[0].line1,

                "Line2": user.addressList[0].line2,

                "Line3": user.addressList[0].province.description,

                "Line4": "N/a", // defaulted

                "PostCode": user.addressList[0].postalCode,

                "SuburbName": user.addressList[0].suburb

            },

            "vehicles": {

                "Colour": null,

                "Make": null,

                "MmCode": null,

                "Model": null,

                "NatisNumber": "#", 

                "RegdrvIdNo": null,

                "RegdrvName": null,

                "RegdrvRelationship": null, 

                "RegdrvMaritalStatus": null,

                "AgreedOrRetail": null, 

                "RegistrationNumber": null,

                "RegularDriverSelfYN": null, 

                "VehicleStatus": null,

                "VehicleType": null,

                "VehicleTypeDesc": null,

                "VinNumber": null,

                "Year": null,

                "LicenseCode": null,

                "LicenseYear": null,

                "AccessoriesValue": null, // defaulted

                "AccessoriesDesc": null, // defaulted 

                "AccessoriesYn": null, // defaulted

                "TotalSumInsured": null, 
                "NoOfClaimsMotor": $("#NumberOfClaimsMotor").val(),
                "CoverType": $("#CoverTypeMotor").val()
            },

            "items": {

                "ItemTypeCodeMotor": "007",
                "ItemTypeCodeContents": "002",
                "ItemTypeCodePersonalBelongings": "004",
                "ItemTypeAllRiskModel": "004",

                "SumInsuredMotor": null,
                "SumInsuredContents": $("#SumInsuredContent").val(),
                "SumInsuredPersonalBelogings": $("#SumInsuredItem").val(),
                "SumInsuredAllRisk": $("#SumInsuredItem").val(),
                "ItemCategory": $("#ItemCategory").val(),
                "allRiskCategory": $("#ItemCategory").val(),
                "NumberOfClaimsContent": $("#NumberOfClaimsContents").val(), // user entry

                "SumInsured": null,
                "ObjectNo": null
            },

            "PropertySituation": "2"
        }
    }
    else {
        vehicle = JSON.parse(localStorage.getItem("vehicle"));
        quoteData = {

            "ID_number": user.identityNumber1,

            "FirstName": user.forename1,

            "LastName": user.surname,

            "Initials": userSignup.Initials,

            "CellNumber": "+27" + user.cellNumber.substring(user.cellNumber.length - 9, user.cellNumber.length),

            "CreditCheckDate": "15-Jun-2021",

            "CreditScore": user.creditScore,

            "DateOfBirth": user.dateOfBirth.substring(0, 10),

            "Email": "email@email.com",

            "Gender": user.gender,

            "HomePhone": "+27" + user.cellNumber.substring(user.cellNumber.length - 9, user.cellNumber.length),

            "JudgementAmount": user.judgementsAmount,

            "LapseScore": user.lapseScore,

            "MaritalStatus": "1",//user.maritalStatus.code,

            "NoOfClaims": 0,  // user entry

            "NoOfJudgements": user.noOfJudgements,

            "PrevInsuranceYears": 14, // user entry

            "RetiredYN": "N", // user entry

            "Title": "004",

            "WorkPhone": "+27" + user.cellNumber.substring(user.cellNumber.length - 9, user.cellNumber.length),

            "address": {

                "AreaCode": user.addressList[0].province.code,

                "CountryCode": "ZA",

                "Line1": user.addressList[0].line1,

                "Line2": user.addressList[0].line2,

                "Line3": user.addressList[0].province.description,

                "Line4": "N/a", // defaulted

                "PostCode": user.addressList[0].postalCode,

                "SuburbName": user.addressList[0].suburb

            },

            "vehicles": {

                "Colour": vehicle.colourCode,

                "Make": vehicle.vehicleMake,

                "MmCode": vehicle.mmCode,

                "Model": vehicle.vehicleModel,

                "NatisNumber": "#", // defaulted entry

                "RegdrvIdNo": user.identityNumber1, // defaulted to the current user

                "RegdrvName": null,

                "RegdrvRelationship": null, // defaulted to the current user

                "RegdrvMaritalStatus": user.maritalStatus.code,

                "AgreedOrRetail": "A", // defaulted entry to retail price

                "RegistrationNumber": $("#VehicleRegNumber").val(),

                "RegularDriverSelfYN": "Y", // defaulted to the current user

                "VehicleStatus": $("#VehicleStatus").val(),

                "VehicleType": vehicle.vehicleTypeCode,

                "VehicleTypeDesc": vehicle.vehicleType,

                "VinNumber": vehicle.vehicleVinNumber,

                "Year": vehicle.vehicleYear,

                "LicenseCode": $("#LicenseCode").val(),

                "LicenseYear": $("#LicenseYear").val(),

                "AccessoriesValue": 0, // defaulted

                "AccessoriesDesc": null, // defaulted 

                "AccessoriesYn": "N", // defaulted

                "TotalSumInsured": vehicle.retailPrice, // defaulted to retail price
                "NoOfClaimsMotor": $("#NumberOfClaimsMotor").val(),
                "CoverType": $("#CoverTypeMotor").val()
            },

            "items": {

                "ItemTypeCodeMotor": "007",
                "ItemTypeCodeContents": "002",
                "ItemTypeCodePersonalBelongings": "004",
                "ItemTypeAllRiskModel": "004",

                "SumInsuredMotor": vehicle.retailPrice,
                "SumInsuredContents": $("#SumInsuredContent").val(),
                "SumInsuredPersonalBelogings": $("#SumInsuredItem").val(),
                "SumInsuredAllRisk": $("#SumInsuredItem").val(),
                "ItemCategory": $("#ItemCategory").val(),
                "allRiskCategory": $("#ItemCategory").val(),
                "NumberOfClaimsContent": $("#NumberOfClaimsContents").val(), // user entry

                "SumInsured": null,
                "ObjectNo": null
            },

            "PropertySituation": "2"
        }
    }

    

    console.log(quoteData);

	$.ajax({
        'data': JSON.stringify(quoteData),
		'contentType': 'application/json; charset=UTF-8',
		'type': 'post',
		'async': true,
		//'dataType': 'json',  
		url: "https://services.qa.ominsure.co.za:8025/binaries/api/GetQuote",
        success: function (response) {
            console.log(response);
            hideLoadingModal();
            document.getElementById('quotemodal').style.display = 'block';
            $("#premium").html("<b>Your quoted premium is " + response.substring((response.indexOf("-") + 1), response.length) + " p/m </b>");
            clearFields();
		},
		error: function (response) {
            showMessageCard("w3-red", "Internal error", "");
            hideLoadingModal();
		}
	});
}

function acceptQuote() {
    openPage('landing');
    showMessageCard("w3-green", "Quote Accepted", "");
    document.getElementById('quotemodal').style.display = 'none';
    localStorage.setItem("vehicle", "");
}

function declineQuote() {
    openPage('landing');
    showMessageCard("w3-red", "Quote Declined", "");
    document.getElementById('quotemodal').style.display = 'none';
    localStorage.setItem("vehicle", "");
}

function showLoadingModal() {
    $("#loadingModal .w3-modal-content").css("top", "140px");
    $("#loadingModal .w3-modal-content").css("padding", "10px");
    document.getElementById('loadingModal').style.display = 'block';
}
function hideLoadingModal() {
    document.getElementById('loadingModal').style.display = 'none';
}

function clearFields() {
    localStorage.setItem("vehicle", "");
    $("#SumInsuredContent").val("");
    $("#SumInsuredItem").val("");
    $("#SumInsuredItem").val("");
    $("#ItemCategory").val("");
    $("#NumberOfClaimsContents").val("");
}