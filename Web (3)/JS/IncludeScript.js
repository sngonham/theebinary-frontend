﻿$(function () {
	
    $("#register").load("PAGES/signup.html?v=1.0.2");
    $("#landing").load("PAGES/landing.html?v=1.0.4");
    $("#quotetype").load("PAGES/getQuoteType.html?v=1.0.5");
    $("#quotevehicle").load("PAGES/getQuoteVehicle.html?v=1.0.2");
    $("#quoteitem").load("PAGES/getQuoteItem.html?v=1.0.2");
    $("#quotecontent").load("PAGES/getQuoteContent.html?v=1.0.2");
    $("#claimtrack").load("PAGES/claimTrack.html?v=1.0.2");
    $("#claimtrackresult").load("PAGES/claimTrackResult.html?v=1.0.2");
    $("#preinspection").load("PAGES/Preinspection.html?v=1.0.2");
});